package Exercise2;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Selenium_Exercise2 {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://formy-project.herokuapp.com/form");
		
		driver.findElement(By.cssSelector("input[placeholder*='first']")).sendKeys("Jorge Luis");
		driver.findElement(By.cssSelector("input[placeholder*='last']")).sendKeys("Avila Silva");
		driver.findElement(By.cssSelector("input[placeholder*='job']")).sendKeys("Software Engineer");
		driver.findElement(By.cssSelector("input[value*='button-2']")).click();
		driver.findElement(By.id("checkbox-1")).click();
		driver.findElement(By.id("select-menu")).click();
		driver.findElement(By.xpath("//option[@value='1']")).click();
		driver.findElement(By.id("datepicker")).sendKeys("01/16/1998"+Keys.RETURN);
		driver.findElement(By.cssSelector("a[class*='btn-primary']")).click();
	}

}
