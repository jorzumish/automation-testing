package Exercise4;

import static org.junit.Assert.*;
import static org.testng.Assert.assertEquals;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.junit.Test;
import com.excel.lib.util.Xls_Reader;

public class loggin {
	
	@Test
	public void TestExcel() throws IOException, InterruptedException {
		System.setProperty("webdriver.chrome.driver", "D:\\chromedriver.exe");
		WebDriver driver = new ChromeDriver(); 	
		driver.manage().window().maximize();
		driver.get("https://formy-project.herokuapp.com/form");
		WebDriverWait w = new WebDriverWait(driver,5);

		File src = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);;
		FileUtils.copyFile(src, new File("C:\\Users\\JorgeAv98\\OneDrive\\Escritorio\\Before.png"));

		formPage fp = new formPage(driver);
		
		Xls_Reader reader = new Xls_Reader("C:\\Users\\JorgeAv98\\OneDrive\\Escritorio\\DatosExcel.xlsx");
		String sheetName = "Hoja1";
		
		int rowCount = reader.getRowCount(sheetName);
		
		for(int rowNum=2; rowNum<=rowCount; rowNum++) {	
			
			String fName = reader.getCellData(sheetName, "FirstName", rowNum);
			String lName = reader.getCellData(sheetName, "LastName", rowNum);
			String jobT = reader.getCellData(sheetName, "JobTitle", rowNum);
			String date = reader.getCellData(sheetName, "Date", rowNum);	
			
			fp.fName().clear();
			fp.fName().sendKeys(fName);
			fp.lName().clear();
			fp.lName().sendKeys(lName);
			fp.tJob().clear();
			fp.tJob().sendKeys(jobT);
			fp.edu().clear();
			fp.edu().get(0).click();
			fp.sex().clear();
			fp.sex().get(0).click();
			fp.optYears().click();
			fp.SelectYears().clear();
			fp.SelectYears().get(1).click();

			fp.date().clear();
			fp.date().sendKeys(date+Keys.RETURN);

		}
		File src2 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);;

		FileUtils.copyFile(src2, new File("C:\\Users\\JorgeAv98\\OneDrive\\Escritorio\\After.png"));
		Thread.sleep(1000);
		fp.submit().click();
		w.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div[role='alert']")));
		assertEquals(driver.findElement(By.cssSelector("[align='center']")).getText(), "Thanks for submitting your form");
		Thread.sleep(1000);
		File src3 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);;
		FileUtils.copyFile(src3, new File("C:\\Users\\JorgeAv98\\OneDrive\\Escritorio\\Completed.png"));
	    }
}
