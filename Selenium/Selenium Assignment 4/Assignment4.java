package Exercise4;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class formPage {
	
	WebDriver driver;
	public formPage(WebDriver driver) {
		this.driver = driver;
	}
	By fName = By.cssSelector("input[placeholder*='first']");
	By lName = By.cssSelector("input[placeholder*='last");
	By tJob = By.cssSelector("input[placeholder*='job']");
	By edu = By.cssSelector("input[type='radio']");
	By sex = By.cssSelector("input[type='checkbox']");
	By selYears = By.id("select-menu");
	By years = By.xpath("//select/option");
	By date = By.id("datepicker");
	By submit = By.cssSelector("a[class*='btn-primary']");
	By message = By.cssSelector("div[role='alert']");
	
	public WebElement fName() {
		return driver.findElement(fName);
	}
	public WebElement lName() {
		return driver.findElement(lName);
	}
	public WebElement tJob() {
		return driver.findElement(tJob);
	}
	public List<WebElement> edu() {
		return driver.findElements(edu);
	}
	public List<WebElement> sex() {
		return driver.findElements(sex);
	}
	public List<WebElement> SelectYears() {
		return driver.findElements(years);
	}
	public WebElement optYears() {
		return driver.findElement(selYears);
	}
	public WebElement date() {
		return driver.findElement(date);
	}
	public WebElement submit() {
		return driver.findElement(submit);
	}
	public WebElement message() {
		return driver.findElement(message);
	}
}
///////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////
package Exercise4;

import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import static org.testng.Assert.assertEquals;

public class loggin {
	@Test
	public void testLoggin() {
		System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");
		WebDriver driver = new ChromeDriver(); 	
		driver.manage().window().maximize();
		driver.get("https://formy-project.herokuapp.com/form");
		
		WebDriverWait w = new WebDriverWait(driver,5);
		
		formPage fp = new formPage(driver);
		fp.fName().sendKeys("Jorge Luis");
		fp.lName().sendKeys("Avila Silva");
		fp.tJob().sendKeys("Automation Testing");
		fp.edu().get(0).click();
		fp.sex().get(2).click();
		fp.optYears().click();
		fp.SelectYears().get(0).click();
		fp.date().sendKeys("01/16/1998"+Keys.RETURN);
		fp.submit().click();
		w.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div[role='alert']")));
		assertEquals(driver.findElement(By.cssSelector("[align='center']")).getText(), "Thanks for submitting your form");
	}
}
